FROM ubuntu:22.04

ARG BUILD_DEPS="g++ gcc gdebi-core make software-properties-common wget"
ARG DEPS="zlib1g-dev"

# Install R and all dependencies

RUN apt-get update \
    && apt-get install --no-install-recommends -y ${BUILD_DEPS} ${DEPS} \
    && wget -qO- 'https://keyserver.ubuntu.com/pks/lookup?op=get&search=0xe298a3a825c0d65dfd57cbb651716619e084dab9' > /etc/apt/trusted.gpg.d/r-project.asc \
    && add-apt-repository 'deb https://cloud.r-project.org/bin/linux/ubuntu jammy-cran40/' \
    && apt-get update \
    && apt-get install --no-install-recommends -y r-base \
    && rm -rf /var/lib/apt/lists/*

# Install Shiny

WORKDIR /tmp/install/

# This also creates a shiny user
RUN export MAKE="make -j $(nproc)" \
    && R -e "install.packages('shiny', repos='http://cran.rstudio.com/')"

# Install Shiny Server

RUN SHINY_SERVER_VERSION=$(wget -qO- https://download3.rstudio.org/ubuntu-18.04/x86_64/VERSION) \
    && wget "https://download3.rstudio.org/ubuntu-18.04/x86_64/shiny-server-${SHINY_SERVER_VERSION}-amd64.deb" \
    && yes | gdebi shiny-server-${SHINY_SERVER_VERSION}-amd64.deb \
    && rm shiny-server-${SHINY_SERVER_VERSION}-amd64.deb

# Clean Up

RUN apt-get purge -y ${BUILD_DEPS} \
    && apt-get autoremove -y

RUN rm -r /tmp/install

# Setup

RUN chown -R shiny:shiny /var/lib/shiny-server/

WORKDIR /home/shiny/

EXPOSE 3838
CMD ["shiny-server"]
